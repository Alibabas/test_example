# README #

# Схема  БД в папке DataBase #

### 1. Для заданного списка товаров получить названия всех категорий, в которых представлены товары.Выборка для нескольких товаров (пример: ids = (9, 14, 6, 7, 2) ). ### 

### 2. Для заданной категории получить список предложений всех товаров из этой категории. Каждая категория может иметь несколько подкатегорий.Пример:Выбираю все товары из категории компьютеры (id = 2) и подкатегории (id =3 (ноутбуки), id = 4 (планшеты), id = 5 (гибриды) ).

### 3. Для заданного списка категорий получить количество уникальных товаров в каждой категории.Выборка для нескольких категорий (пример: ids = (2, 3, 4) ). ###

### 4. Для заданного списка категорий получить количество единиц каждого товара который входит в указанные категории.Выборка для нескольких категорий (пример: ids = (3, 4, 5) ) ###


### База данных ###

CREATE TABLE products(	
			product_id INTEGER PRIMARY KEY, 
 			category_id INT NOT NULL,
			name TEXT UNIQUE, 
			quantity INTEGER);

CREATE TABLE category(	
			category_id INTEGER PRIMARY KEY, 
			title TEXT UNIQUE, 
			tree_root INTEGER DEFAULT NULL,
			parent_id INTEGER DEFAULT NULL,
			lft INTEGER NOT NULL,
			lvl INTEGER NOT NULL,
			rgt INTEGER NOT NULL);
			
ALTER TABLE category ADD CONSTRAINT FK_3AF34668 FOREIGN KEY (tree_root) REFERENCES category(category_id);   
ALTER TABLE category ADD CONSTRAINT FK_3AF6238A97 FOREIGN KEY (parent_id) REFERENCES category(category_id);   
ALTER TABLE category ADD INDEX(tree_root);   
ALTER TABLE category ADD INDEX(parent_id);    
ALTER TABLE products ADD CONSTRAINT FK_3AF34668B8 FOREIGN KEY (category_id) REFERENCES category(category_id);    


INSERT INTO category VALUES (1, 'Computers and Electronics', 1, NULL, 1, 0, 18);

INSERT INTO category VALUES (2, 'Computers', 1, 1, 2, 1, 9);

INSERT INTO category VALUES (3, 'Laptops', 1, 2, 3, 2, 4);   
INSERT INTO category VALUES (4, 'Tablets', 1, 2, 5, 2, 6);   
INSERT INTO category VALUES (5, 'Hybrids', 1, 2, 7, 2, 8);  

INSERT INTO category VALUES (6, 'Phones', 1, 1, 10, 1, 17);   

INSERT INTO category VALUES (7, 'Smartphones', 1, 2, 11, 2, 12);   
INSERT INTO category VALUES (8, 'Phone accessories', 1, 2, 13, 2, 14);   
INSERT INTO category VALUES (9, 'Office phones', 1, 2, 15, 2, 16);  

INSERT INTO category VALUES (10, 'Clothing', 10, NULL, 1, 0, 10);  

INSERT INTO category VALUES (11, 'Baby clothes', 10, 10, 2, 1, 9);      

INSERT INTO category VALUES (12, 'T-shirts', 10, 11, 3, 2, 4);  
INSERT INTO category VALUES (13, 'Tracksuits', 10, 11, 5, 2, 6);    
INSERT INTO category VALUES (14, 'Dresses', 10, 11, 7, 2, 8);   


INSERT INTO products VALUES (1, 3, 'Laptop-1', 4);  
INSERT INTO products VALUES (2, 3, 'Laptop-2', 8);  
INSERT INTO products VALUES (3, 3, 'Laptop-3', 5);  
INSERT INTO products VALUES (4, 3, 'Laptop-4', 3);		  
INSERT INTO products VALUES (5, 3, 'Laptop-5', 11); 
INSERT INTO products VALUES (6, 3, 'Laptop-6', 7);	  
INSERT INTO products VALUES (7, 3, 'Laptop-7', 9);  


INSERT INTO products VALUES (8, 4, 'Tablet-1', 5);  
INSERT INTO products VALUES (9, 4, 'Tablet-2', 1);  
INSERT INTO products VALUES (10, 4, 'Tablet-3', 2); 
INSERT INTO products VALUES (11, 4, 'Tablet-4', 6);		 
INSERT INTO products VALUES (12, 4, 'Tablet-5', 12);		    

INSERT INTO products VALUES (13, 5, 'Hybrid-1', 9); 
INSERT INTO products VALUES (14, 5, 'Hybrid-2', 1);		 
INSERT INTO products VALUES (15, 5, 'Hybrid-3', 14);		    
INSERT INTO products VALUES (16, 5, 'Hybrid-4', 17);    
INSERT INTO products VALUES (17, 5, 'Hybrid-5', 2); 

INSERT INTO products VALUES (18, 7, 'Smartphone-1', 5); 
INSERT INTO products VALUES (19, 7, 'Smartphone-2', 3); 
INSERT INTO products VALUES (20, 7, 'Smartphone-3', 6); 
INSERT INTO products VALUES (21, 7, 'Smartphone-4', 7); 
INSERT INTO products VALUES (22, 7, 'Smartphone-5', 20);    
INSERT INTO products VALUES (23, 7, 'Smartphone-6', 19);    
INSERT INTO products VALUES (24, 7, 'Smartphone-7', 1); 

INSERT INTO products VALUES (25, 8, 'Phone accessory-1', 7);    
INSERT INTO products VALUES (26, 8, 'Phone accessory-2', 8);    
INSERT INTO products VALUES (27, 8, 'Phone accessory-3', 9);    
INSERT INTO products VALUES (28, 8, 'Phone accessory-4', 2);    
INSERT INTO products VALUES (29, 8, 'Phone accessory-5', 3);    

INSERT INTO products VALUES (30, 9, 'Office phone-1', 4);   
INSERT INTO products VALUES (31, 9, 'Office phone-2', 14);  
INSERT INTO products VALUES (32, 9, 'Office phone-3', 23);  
INSERT INTO products VALUES (33, 9, 'Office phone-4', 55);  
INSERT INTO products VALUES (34, 9, 'Office phone-5', 17);  

INSERT INTO products VALUES (35, 12, 'T-shirt-1', 13);  
INSERT INTO products VALUES (36, 12, 'T-shirt-2', 18);  
INSERT INTO products VALUES (37, 12, 'T-shirt-3', 52);  
INSERT INTO products VALUES (38, 12, 'T-shirt-4', 41);  
INSERT INTO products VALUES (39, 12, 'T-shirt-5', 37);  
INSERT INTO products VALUES (40, 12, 'T-shirt-6', 15);  

INSERT INTO products VALUES (41, 13, 'Tracksuit-1', 11);    
INSERT INTO products VALUES (42, 13, 'Tracksuit-2', 15);    
INSERT INTO products VALUES (43, 13, 'Tracksuit-3', 23);    
INSERT INTO products VALUES (44, 13, 'Tracksuit-4', 25);    
INSERT INTO products VALUES (45, 13, 'Tracksuit-5', 20);    
INSERT INTO products VALUES (46, 13, 'Tracksuit-6', 10);    

INSERT INTO products VALUES (47, 14, 'Dress-1', 31);    
INSERT INTO products VALUES (48, 14, 'Dress-2', 54);    
INSERT INTO products VALUES (49, 14, 'Dress-3', 1); 
INSERT INTO products VALUES (50, 14, 'Dress-4', 21);    
INSERT INTO products VALUES (51, 14, 'Dress-5', 43);    

### Повторяющиеся элементы ###

INSERT INTO products VALUES (52, 3, 'Laptop-3', 9); 
INSERT INTO products VALUES (53, 3, 'Laptop-3', 1); 
INSERT INTO products VALUES (54, 3, 'Laptop-5', 144);   
INSERT INTO products VALUES (55, 3, 'Laptop-5', 143);   
INSERT INTO products VALUES (56, 3, 'Laptop-5', 8); 

INSERT INTO products VALUES (57, 4, 'Tablet-1', 88);    
INSERT INTO products VALUES (58, 4, 'Tablet-1', 34);    
INSERT INTO products VALUES (59, 4, 'Tablet-3', 54);    
INSERT INTO products VALUES (60, 4, 'Tablet-3', 13);    
INSERT INTO products VALUES (61, 4, 'Tablet-3', 17);    

INSERT INTO products VALUES (62, 12, 'T-shirt-3', 31);  
INSERT INTO products VALUES (63, 12, 'T-shirt-3', 32);  
INSERT INTO products VALUES (64, 12, 'T-shirt-3', 33);  
INSERT INTO products VALUES (65, 12, 'T-shirt-1', 31);  

INSERT INTO products VALUES (66, 12, 'T-shirt-1', 13);  

INSERT INTO products VALUES (67, 12, 'T-shirt-1', 44);  


## Решения ##

### 1.) ###
CREATE TEMPORARY TABLE cat_hir  
SELECT c1.*, p.product_id, p.name FROM category c1  
LEFT JOIN products p ON p.category_id = c1.category_id  
 WHERE p.product_id IN (1, 8, 25,47, 38);

INSERT INTO cat_hir     
SELECT  c2.*, c1.product_id, c1.name FROM category c2   
INNER JOIN cat_hir c1   
       WHERE c2.lft < c1.lft AND c2.rgt > c1.rgt    
       AND c2.tree_root = c1.tree_root  
       ORDER BY c2.rgt-c1.rgt ASC;  

SELECT product_id, name, title, category_id FROM cat_hir    
ORDER BY name   
			 
			 
			 
			
### 2.) ###
CREATE TEMPORARY TABLE cid  
SELECT * FROM category c1   
WHERE c1.category_id = 10;  

INSERT INTO cid     
SELECT  c2.* FROM category c2   
INNER JOIN cid c1   
       WHERE c2.lft > c1.lft AND c2.rgt < c1.rgt    
       AND c2.tree_root = c1.tree_root; 

SELECT p.product_id, p.name, cat.category_id, cat.title FROM products p 
INNER JOIN category cat ON cat.category_id = p.category_id  
WHERE cat.category_id IN    
( SELECT category_id  FROM cid) 
ORDER BY cat.title  

### 3.) ###
CREATE TEMPORARY TABLE uniq_p   
 SELECT * FROM category c1  
 WHERE c1.category_id IN (2,3,4,12);    

INSERT INTO uniq_p      
SELECT  c2.category_id, c1.title, c2.tree_root, c2.parent_id, c2.lft, c2.lvl, c2.rgt FROM category c2      
INNER JOIN uniq_p c1    
       WHERE c2.lft > c1.lft AND c2.rgt < c1.rgt    
       AND c2.tree_root = c1.tree_root; 

SELECT p.product_id, p.name, cat.title FROM products p 
INNER JOIN uniq_p cat ON cat.category_id = p.category_id    
WHERE cat.category_id IN    
( SELECT category_id  FROM uniq_p)  
GROUP BY p.name 

### Тут я немного не понял. Нам нужно вывести кол-во повторяющихся товаров в категории или кол-во товаров на складе. Сделал оба варианта ###
### 4.1) Кол-во повторяющихся товаров в категории ###
CREATE TEMPORARY TABLE p_quant  
SELECT * FROM category c1   
WHERE c1.category_id IN (2,3,4,12); 

INSERT INTO p_quant
SELECT  c2.category_id, c1.title, c2.tree_root, c2.parent_id, c2.lft, c2.lvl, c2.rgt FROM category c2
INNER JOIN p_quant c1       
       WHERE c2.lft > c1.lft AND c2.rgt < c1.rgt     
       AND c2.tree_root = c1.tree_root;  

SELECT COUNT(p.product_id) AS quantity_product , p.name, cat.title FROM products p  
INNER JOIN p_quant cat ON cat.category_id = p.category_id     
WHERE cat.category_id IN    
( SELECT category_id  FROM p_quant) 
GROUP BY p.name 
ORDER BY quantity_product ASC   

### 4.2) Кол-во товаров на складе ###


CREATE TEMPORARY TABLE p_quant  
SELECT * FROM category c1   
WHERE c1.category_id IN (3, 4, 5, 11);  

INSERT INTO p_quant 
 SELECT  c2.category_id, c1.title, c2.tree_root, c2.parent_id, c2.lft, c2.lvl, c2.rgt FROM category c2  
 INNER JOIN p_quant c1  
       WHERE c2.lft > c1.lft AND c2.rgt < c1.rgt    
       AND c2.tree_root = c1.tree_root; 

SELECT MAX(p.quantity), p.name, cat.title FROM products p   
INNER JOIN p_quant cat ON cat.category_id = p.category_id   
WHERE cat.category_id IN    
( SELECT category_id  FROM p_quant) 
GROUP BY p.name 