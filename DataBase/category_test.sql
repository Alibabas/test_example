-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Дек 11 2020 г., 03:47
-- Версия сервера: 10.3.22-MariaDB-1:10.3.22+maria~bionic-log
-- Версия PHP: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `category_test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `title` text NOT NULL,
  `tree_root` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `lft` int(11) NOT NULL,
  `lvl` int(11) NOT NULL,
  `rgt` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`category_id`, `title`, `tree_root`, `parent_id`, `lft`, `lvl`, `rgt`) VALUES
(1, 'Computers and Electronics', 1, NULL, 1, 0, 18),
(2, 'Computers', 1, 1, 2, 1, 9),
(3, 'Laptops', 1, 2, 3, 2, 4),
(4, 'Tablets', 1, 2, 5, 2, 6),
(5, 'Hybrids', 1, 2, 7, 2, 8),
(6, 'Phones', 1, 1, 10, 1, 17),
(7, 'Smartphones', 1, 2, 11, 2, 12),
(8, 'Phone accessories', 1, 2, 13, 2, 14),
(9, 'Office phones', 1, 2, 15, 2, 16),
(10, 'Clothing', 10, NULL, 1, 0, 10),
(11, 'Baby clothes', 10, 10, 2, 1, 9),
(12, 'T-shirts', 10, 11, 3, 2, 4),
(13, 'Tracksuits', 10, 11, 5, 2, 6),
(14, 'Dresses', 10, 11, 7, 2, 8);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `product_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `name` text NOT NULL,
  `quantity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`product_id`, `category_id`, `name`, `quantity`) VALUES
(1, 3, 'Laptop-1', 4),
(2, 3, 'Laptop-2', 8),
(3, 3, 'Laptop-3', 5),
(4, 3, 'Laptop-4', 3),
(5, 3, 'Laptop-5', 11),
(6, 3, 'Laptop-6', 7),
(7, 3, 'Laptop-7', 9),
(8, 4, 'Tablet-1', 5),
(9, 4, 'Tablet-2', 1),
(10, 4, 'Tablet-3', 2),
(11, 4, 'Tablet-4', 6),
(12, 4, 'Tablet-5', 12),
(13, 5, 'Hybrid-1', 9),
(14, 5, 'Hybrid-2', 1),
(15, 5, 'Hybrid-3', 14),
(16, 5, 'Hybrid-4', 17),
(17, 5, 'Hybrid-5', 2),
(18, 7, 'Smartphone-1', 5),
(19, 7, 'Smartphone-2', 3),
(20, 7, 'Smartphone-3', 6),
(21, 7, 'Smartphone-4', 7),
(22, 7, 'Smartphone-5', 20),
(23, 7, 'Smartphone-6', 19),
(24, 7, 'Smartphone-7', 1),
(25, 8, 'Phone accessory-1', 7),
(26, 8, 'Phone accessory-2', 8),
(27, 8, 'Phone accessory-3', 9),
(28, 8, 'Phone accessory-4', 2),
(29, 8, 'Phone accessory-5', 3),
(30, 9, 'Office phone-1', 4),
(31, 9, 'Office phone-2', 14),
(32, 9, 'Office phone-3', 23),
(33, 9, 'Office phone-4', 55),
(34, 9, 'Office phone-5', 17),
(35, 12, 'T-shirt-1', 13),
(36, 12, 'T-shirt-2', 18),
(37, 12, 'T-shirt-3', 52),
(38, 12, 'T-shirt-4', 41),
(39, 12, 'T-shirt-5', 37),
(40, 12, 'T-shirt-6', 15),
(41, 13, 'Tracksuit-1', 11),
(42, 13, 'Tracksuit-2', 15),
(43, 13, 'Tracksuit-3', 23),
(44, 13, 'Tracksuit-4', 25),
(45, 13, 'Tracksuit-5', 20),
(46, 13, 'Tracksuit-6', 10),
(47, 14, 'Dress-1', 31),
(48, 14, 'Dress-2', 54),
(49, 14, 'Dress-3', 1),
(50, 14, 'Dress-4', 21),
(51, 14, 'Dress-5', 43),
(52, 3, 'Laptop-3', 9),
(53, 3, 'Laptop-3', 1),
(54, 3, 'Laptop-5', 144),
(55, 3, 'Laptop-5', 143),
(56, 3, 'Laptop-5', 8),
(57, 4, 'Tablet-1', 88),
(58, 4, 'Tablet-1', 34),
(59, 4, 'Tablet-3', 54),
(60, 4, 'Tablet-3', 13),
(61, 4, 'Tablet-3', 17),
(62, 12, 'T-shirt-3', 31),
(63, 12, 'T-shirt-3', 32),
(64, 12, 'T-shirt-3', 33),
(65, 12, 'T-shirt-1', 31),
(66, 12, 'T-shirt-1', 13),
(67, 12, 'T-shirt-1', 44);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`),
  ADD KEY `tree_root` (`tree_root`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`product_id`),
  ADD KEY `FK_3AF34668B8` (`category_id`);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `FK_3AF34668` FOREIGN KEY (`tree_root`) REFERENCES `category` (`category_id`),
  ADD CONSTRAINT `FK_3AF6238A97` FOREIGN KEY (`parent_id`) REFERENCES `category` (`category_id`);

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `FK_3AF34668B8` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
