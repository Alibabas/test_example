<?php

namespace App\Data;


interface ISetTasks
{

    public function setTasks(): string;

}