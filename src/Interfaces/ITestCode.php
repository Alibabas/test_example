<?php

namespace App\Data;


interface ITestCode
{

    public function testCode(): string;

}