<?php

namespace App\Data;


interface IWriteCode
{

    public function writeCode(): string;

}