<?php


namespace App\Interfaces;


abstract class Company
{

    /**
     * @param string $class
     * @return Employee
     */
    abstract public function hiringAnEmployeeFactory($class): Employee;


}