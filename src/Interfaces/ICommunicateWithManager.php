<?php

namespace App\Data;


interface ICommunicateWithManager
{

    public function communicateWithManager(): string;

}