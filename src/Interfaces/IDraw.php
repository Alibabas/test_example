<?php

namespace App\Data;


interface IDraw
{

    public function draw(): string ;

}