<?php

namespace App\Data;


use App\Interfaces\Employee;

class Manager implements
    ISetTasks,
    Employee
{

    public function doWork()
    {
        $skillList = [];
        $skillList[] = $this->setTasks();

        return $skillList;
    }

    public function setTasks(): string
    {
        return '- setting tasks';
    }
}