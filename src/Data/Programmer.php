<?php

namespace App\Data;


use App\Interfaces\Employee;

class Programmer implements
    IWriteCode,
    ITestCode,
    ICommunicateWithManager,
    Employee
{

    public function doWork()
    {
        $skillList = [];
        $skillList[] = $this->communicateWithManager();
        $skillList[] = $this->testCode();
        $skillList[] = $this->writeCode();

        return $skillList;
    }

    public function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    public function testCode(): string
    {
        return '- code testing';
    }

    public function writeCode(): string
    {
        return '- code writing';
    }
}