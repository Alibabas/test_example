<?php

namespace App\Data;


use App\Interfaces\Employee;

class Tester implements
    ITestCode,
    ICommunicateWithManager,
    ISetTasks,
    Employee
{

    public function doWork()
    {
        $skillList = [];
        $skillList[] = $this->communicateWithManager();
        $skillList[] = $this->testCode();
        $skillList[] = $this->setTasks();

        return $skillList;
    }

    public function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    public function setTasks(): string
    {
        return '- setting tasks';
    }

    public function testCode(): string
    {
        return '- code testing';
    }
}