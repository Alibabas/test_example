<?php

namespace App\Data;


use App\Interfaces\Employee;

class Designer implements
    IDraw,
    ICommunicateWithManager,
    Employee
{

    public function doWork()
    {
        $skillList = [];
        $skillList[] = $this->communicateWithManager();
        $skillList[] = $this->draw();

        return $skillList;
    }

    public function communicateWithManager(): string
    {
        return '- communication with manager';
    }

    public function draw(): string
    {
        return '- drawing';
    }
}