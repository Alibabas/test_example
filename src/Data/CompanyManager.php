<?php

namespace App\Data;


use App\Interfaces\Employee;


class CompanyManager extends \App\Interfaces\Company
{

    /**
     * @param string $class
     * @return Employee
     */
    public function hiringAnEmployeeFactory($class): Employee
    {

        return new $class();

    }
}