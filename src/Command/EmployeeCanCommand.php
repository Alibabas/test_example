<?php

namespace App\Command;

use App\Data\CompanyManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class EmployeeCanCommand extends Command
{
    protected static $defaultName = 'employee:can';

    protected function configure()
    {
        $this
            ->setDescription('Employee skills test')
            ->addArgument('employee', InputArgument::REQUIRED, 'Company employee')
            ->addArgument('skill', InputArgument::REQUIRED, 'Employee skill')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $employee = $input->getArgument('employee');
        $skill = $input->getArgument('skill');

//        фабричный метод
        $manager = new CompanyManager();

        $employee = 'App\Data\\' . ucfirst(strtolower($employee));

        if(class_exists($employee)) {
            $person = $manager->hiringAnEmployeeFactory($employee);

        } else {
            throw new \RuntimeException('We do not have such employee');
        }

        $checkSkill = method_exists($person, $skill) ? 'true': 'false';

        $io->writeln($checkSkill);

        return 0;
    }

}
