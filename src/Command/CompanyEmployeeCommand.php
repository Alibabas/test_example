<?php

namespace App\Command;

use App\Data\CompanyManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CompanyEmployeeCommand extends Command
{
    protected static $defaultName = 'company:employee';

    protected function configure()
    {
        $this
            ->setDescription('Shows employee skills')
            ->addArgument('specialty', InputArgument::REQUIRED, 'Worker specialty')
            ->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $specialty = $input->getArgument('specialty');

        //        фабричный метод
        $manager = new CompanyManager();

        $specialty = 'App\Data\\' . ucfirst(strtolower($specialty));

        if(class_exists($specialty)) {
            $person = $manager->hiringAnEmployeeFactory($specialty);

        } else {
            throw new \RuntimeException('We do not have such a specialist');
        }

        $skillList = $person->doWork();

        $rows = [];
        foreach ($skillList as $val) {
            $rows[] = [$val];
        }

        $io->table(['Skills'], $rows);
//        $io->listing($skillList);

        return 0;
    }
}
